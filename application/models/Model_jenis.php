<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_jenis extends CI_Model {

	public function get_insert($data) {
		$qry_insert = $this->db->insert('tb_jenis', $data);

		return $qry_insert;
	}

	public function get_update($key,$data) {
		$qry_update = $this->db->where('id', $key)->update('tb_jenis', $data);

		return $qry_update;
	}

	public function get_delete($key) {
		$qry_delete = $this->db->where('kd_jenis', $key)->delete('tb_jenis');

		return $qry_delete;
	}
}
