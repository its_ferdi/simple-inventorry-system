<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_barang extends CI_Model {

	public function get_insert($data) {
		$qry_insert = $this->db->insert('tb_barang', $data);

		return $qry_insert;
	}

	public function get_update($key,$data) {
		$this->db->where('id', $key);
		$this->db->set('updated_at', 'NOW()', false);
		$qry_update = $this->db->update('tb_barang', $data);

		return $qry_update;
	}

	public function get_delete($key) {
		$qry_delete = $this->db->where('kd_barang', $key)->delete('tb_barang');

		return $qry_delete;
	}
}
