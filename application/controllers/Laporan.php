<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->model('model_laporan');
	}

	function index()
	{
		$data['konten'] 	= 'halaman/laporan/index';

		$this->load->view('home', $data);
	}

	public function get_data() {
		$data['data']	= $this->db->select('*')->from('tb_laporan')->order_by('id', 'asc')->get()->result();

		/*foreach ($qry as $kolom) {
			$tmp['id'] 		= $kolom->id;
			$tmp['nama']	= $kolom->nama;
			$tmp['nominal']	= number_format($kolom->nominal, 0,',','');
			$tmp['debit']	= number_format($kolom->debit, 0, ',','');
			$tmp['kredit']	= number_format($kolom->kredit, 0, ',',''); 

		}*/
			echo json_encode($data);

	}

	public function insert() {
		$data['nama'] 			= $this->input->post('nama');
		$data['nominal'] 		= $this->input->post('nominal');
		$pembayaran 			= $this->input->post('pembayaran');

		if($pembayaran == 'debit') {
			$data['debit']		= $this->input->post('nominal');
			$data['kredit']		= 0;
		}else{
			$data['debit']		= 0;
			$data['kredit']		= $this->input->post('nominal');
		}

		$qry 	= $this->model_laporan->get_insert($data);

		if($qry) {
			echo json_encode(1);
		}
	}

	public function print_pdf(){

		$data['data'] 	= $this->db->get('tb_laporan')->result();
		$cetak 			= $this->load->view('halaman/laporan/tabel', $data, true);

		$pdfFilePath = date('dd').".pdf";
 
        //load mPDF library
        $this->load->library('m_pdf');

        $stylesheet = file_get_contents(base_url().'/assets/dist/lib/css/report.css');
 
        $this->m_pdf->pdf->WriteHTML($stylesheet,1);
        $this->m_pdf->pdf->WriteHTML($cetak);
 
        //download it.
        $this->m_pdf->pdf->Output($pdfFilePath, "D");
	}
}
