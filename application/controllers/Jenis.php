<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->model('model_jenis');
	}

	public function index()
	{
		$data['konten']		= 'halaman/jenis/index.php';

		$this->load->view('home', $data);
	}

	public function get_data() {
		$data['data'] 	= $this->db->get('tb_jenis')->result();

		echo json_encode($data);
	}

	public function insert() {
		$data['kd_jenis']		= $this->input->post('kd_jenis');
		$data['jenis']			= $this->input->post('jenis');

		// print_r($data);
		$qry 	= $this->model_jenis->get_insert($data);

		if($qry) {
			echo json_encode(1);
		}
	}

	public function edit() {
		$data['id']				= $this->input->post('id');
		$data['kd_jenis']		= $this->input->post('kd_jenis');
		$data['jenis']			= $this->input->post('jenis');

		$qry 	= $this->model_jenis->get_update($data['id'],$data);

		if ($qry) {
			echo json_encode(1);
		}
	}

	public function delete() {
		$kd_jenis 		= $this->input->post('kd_jenis');

		$qry 	= $this->model_jenis->get_delete($kd_jenis);

		if($qry) {
			echo json_encode(1);
		}
	}
}
