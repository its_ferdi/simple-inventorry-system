<div class="row">
    <div class="col-xs-8">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    <div class="title"> Cetak Laporan </div>
                </div>
            </div>

            <div class="card-body">
                <form class="form-horizontal" id="form-tambah">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Nama</label>

                        <div class="col-sm-5">
                            <input type="text" name="nama" id="nama" class="form-control input-sm" autofocus required></input>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Nominal</label>

                        <div class="col-sm-5">
                            <input type="text" name="nominal" id="nominal" class="form-control input-sm" required></input>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-3 control-label">
                            <label>Pembayaran</label>
                        </div>

                        <div class="col-sm-6">
                            <div class="radio3 radio-check radio-inline">
                                <input type="radio" id="radio4" name="pembayaran" value="debit" checked="">
                                <label for="radio4">
                                   Debit
                                </label>
                            </div>
                            <div class="radio3 radio-check radio-success radio-inline">
                                <input type="radio" id="radio5" name="pembayaran" value="kredit">
                                <label for="radio5">
                                    Kredit
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-offset-3 col-sm-2">
                            <button type="submit" id="bt_tambah" class="btn btn-primary btn-sm"> Tambah</button>
                        </div>
                    </div>
                </form>
            </div><!-- end card-body -->
        </div><!-- end card -->
    </div><!-- end col-xs-6 -->
</div><!-- end row -->

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                    <div class="form-group">
                        <div class="col-sm-12">
                        <a href="<?php echo base_url() ?>index.php/laporan/print-pdf" class="btn btn-danger btn-sm pull-right"><i class="fa fa-file-pdf-o"></i> PDF</a>
                            <table id="tabel-laporan" class="table table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <th></th>
                                    <th width="200">Nama</th>
                                    <th width="150">Nominal</th>
                                    <th width="150">Debit</th>
                                    <th width="150">Kredit</th>
                                </thead>
                                <tfoot>
                                    <th colspan="3" style="text-align: right;font-size: 12px;">Total (Rp)</th>
                                    <th>0</th>
                                    <th>0</th>
                                </tfoot>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
            </div><!-- end card-body -->
        </div><!-- end card -->
    </div><!-- end col-sm-12 -->
</div><!-- end row -->

<style type="text/css">
    tfoot{
        display: table-footer-group !important;
        position: relative !important; 
    }
    td,th{
        text-align: center;
    }
</style>
<script type="text/javascript" src="<?php echo base_url() ?>assets/app/js/dataTables.editor.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/app/js/laporan.js"></script>