<div class="row">
    <div class="col-xs-6">
        <div class="card">
    
            <div class="card-body">
                <form class="form-horizontal" action="<?php echo base_url() ?>index.php/barang/insert" method="POST">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Kode</label>
                        <div class="col-sm-7">
                            <input type="text" name="kd_barang" class="form-control input-sm" required autofocus></input>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Nama</label>
                        <div class="col-sm-7">
                            <input type="text" name="nm_barang" class="form-control input-sm"  required></input>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Jenis</label>
                        <div class="col-sm-4">
                            <select name="jenis" class="form-control">
                                <option value="kosong"> ... </option>
                                <option value="K001"> Obat </option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary btn-sm">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
</div><!-- end row -->