<div class="row">
    <div class="col-xs-8">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    <div class="title"> Data Barang </div>
                </div>

                <div class="pull-right">
                    <div class="col-sm-6">
                        <button type="button" id="bt_tambah" class="btn btn-primary btn-sm"> <i class="fa fa-plus"></i> Buat baru</button>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <table id="tabel-barang" class="table table-striped" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Kode</th>
                            <th>Nama Barang</th>
                            <th>jenis</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Kode</th>
                            <th>Position</th>
                            <th>Jenis</th>
                            <th></th>
                        </tr>
                    </tfoot>
                    <tbody>
                    </tbody>
                </table>
            </div><!-- end card-body -->
        </div><!-- end card -->
    </div><!-- end col-xs-6 -->
</div><!-- end row -->


<!-- MODAL TAMBAH -->
<div class="modal fade modal-defualt" id="modal-tambah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Buat baru</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-8">
                        <form class="form-horizontal" action="<?php echo base_url() ?>index.php/barang/insert" method="POST">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Kode</label>
                                <div class="col-sm-7">
                                    <input type="text" name="kd_barang" id="kd_barang" class="form-control input-sm" required autofocus></input>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Nama</label>
                                <div class="col-sm-7">
                                    <input type="text" name="nm_barang" id="nm_barang" class="form-control input-sm"  required></input>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Jenis</label>
                                <div class="col-sm-4">
                                    <select name="jenis" id="jenis" class="form-control">
                                        <option value="kosong"> ... </option>
                                        <option value="K001"> Obat </option>
                                    </select>
                                </div>
                            </div>
                    </div><!-- akhir div col-sm-6 -->
                </div><!-- akhir row -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">     
                <i class="fa fa-remove"></i> Batal
                </button>
                <button type="button" id="bt_simpan" class="btn btn-primary">
                    <i class="fa fa-check"></i> Simpan
                </button>
                </form>
            </div>
        </div>
    </div>
</div>


<!-- MODAL EDIT -->
<div class="modal fade modal-default" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Ubah Data</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-8">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Kode</label>
                                <div class="col-sm-7">
                                    <input type="text" name="kd_barang" id="edit_kd_barang" class="form-control input-sm" required autofocus></input>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Nama</label>
                                <div class="col-sm-7">
                                    <input type="text" name="nm_barang" id="edit_nm_barang" class="form-control input-sm"  required></input>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Jenis</label>
                                <div class="col-sm-4">
                                    <select name="jenis" id="edit_jenis" class="form-control">
                                        <option value="kosong"> ... </option>
                                        <option value="K001"> Obat </option>
                                    </select>
                                </div>
                            </div>
                    </div><!-- akhir div col-sm-6 -->
                </div><!-- akhir row -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">     
                <i class="fa fa-remove"></i> Batal
                </button>
                <button type="button" id="bt_edit" class="btn btn-primary">
                    <i class="fa fa-check"></i> Simpan
                </button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal-default" id="modal-hapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
            </div>

            <div class="modal-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label>Apakah anda yakin ingin menghapus data ini ?</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">     
                <i class="fa fa-remove"></i> Tidak
                </button>
                <button type="button" id="bt_delete" class="btn btn-primary">
                    <i class="fa fa-check"></i> Ya
                </button>
            </div>
        </div>
    </div>    
</div>

<style type="text/css">
    .btn-group{
        font-size: 10px !important;
    }
</style>
<script type="text/javascript">
    $(document).ready(function() {
        $('#bt_tambah').on('click', function() {
            $('#modal-tambah').modal('show');
        });

        // Save ke database
        $('#bt_simpan').on('click', function() {
            var kd_barang   = $('#kd_barang').val();
            var nm_barang   = $('#nm_barang').val();
            var jenis       = $('#jenis').val();
            var table       = $('#tabel-barang').DataTable();

            $.ajax({
                url     : 'http://localhost/portofolio/softdroid/index.php/barang/insert',
                data    : 'kd_barang='+kd_barang+'&nm_barang='+nm_barang+'&jenis='+jenis,
                type    : "POST",
                dataType: 'json',
                success : function(data) {

                    if(data == 1) {
                        $('#modal-tambah').modal('hide');
                        $('#kd_barang').val('');
                        $('#nm_barang').val('');
                        $('#jenis').val('kosong');


                        window.setTimeout(function() {
                            table.ajax.reload();
                        }, 2000);
                    }
                }
            })
        });
    });

    var table = $('#tabel-barang').DataTable({
        "processing"    : true,
        "serverside"    : true,
        "ajax"          : {
                            "url"    :  "http://localhost/portofolio/softdroid/index.php/barang/get_data"
                        },
            "columns"       : [
                            {"data" : "kd_barang"},
                            {"data" : "nm_barang"},
                            {"data" : "kd_jenis"},
                            {"defaultContent" : "<div class='btn-group'>"+
                                    "<button type='button' class='btn btn-info dropdown-toggle btn-xs' data-toggle='dropdown' aria-expanded='false'>Pilih aksi  <span class='caret'></span></button>"+
                                    "<ul class='dropdown-menu dropdown-menu-right' role='menu'>"+
                                        "<li><a id='edit' href='javascript:void(0)'><i class='fa fa-pencil'></i> Ubah</a></li>"+
                                        "<li><a id='hapus' onclick='' href='javascript:void(0)'><i class='fa fa-trash'></i> Hapus</a></li>"+
                                        "<li><a href='#'><i class='fa fa-print'></i> Cetak</a></li>"+
                                    "</ul>"+
                                "</div>" },

            ]
    });

    // Show modal edit 
    $('#tabel-barang tbody').on('click','#edit', function() {

        var data = table.row( $(this).parents('tr') ).data();

        $('#modal-edit').modal('show');
        $('#edit_kd_barang').val(data.kd_barang);
        $('#edit_nm_barang').val(data.nm_barang);
        $('#edit_jenis').val(data.kd_jenis);

    });

    // Proces to update data
    $('#bt_edit').on('click', function() {
       var kd_barang    =  $('#edit_kd_barang').val();
       var nm_barang    =  $('#edit_nm_barang').val();
       var jenis        =  $('#edit_jenis').val();

       $.ajax({
            url     : 'http://localhost/portofolio/softdroid/index.php/barang/edit',
            data    : 'kd_barang='+kd_barang+'&nm_barang='+nm_barang+'&jenis='+jenis,
            type    : 'POST',
            dataType: 'json',
            success : function(data) {

                if(data == 1) {

                    $('#modal-edit').modal('hide');
                    // reload table
                    window.setTimeout(function() {
                        table.ajax.reload();
                    }, 2000);
                }
            }
       });
    });

    // Hapus data
    $('#tabel-barang tbody').on('click','#hapus', function() {

        var data = table.row( $(this).parents('tr') ).data();
        $('#modal-hapus').modal('show');

        $('#bt_delete').on('click', function() {

            $.ajax({
                url     : 'http://localhost/portofolio/softdroid/index.php/barang/delete',
                data    : 'kd_barang='+data.kd_barang,
                type    : 'POST',
                dataType: 'json',
                success : function(data) {

                    if(data == 1) {

                        $('#modal-hapus').modal('hide');
                        // reload table
                        window.setTimeout(function() {
                            table.ajax.reload();
                        }, 2000);
                    }
                }
            });
        });
    });
</script>
