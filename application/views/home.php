<!DOCTYPE html>
<html>

<head>
    <title> SOFTDROID </title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300,400' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
    <!-- CSS Libs -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/dist/lib/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/dist/lib/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/dist/lib/css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/dist/lib/css/bootstrap-switch.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/dist/lib/css/checkbox3.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/dist/lib/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/dist/lib/css/dataTables.bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/dist/lib/css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/dist/lib/css/select2.min.css">
    <!-- CSS App -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/dist/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/dist/css/themes/flat-blue.css">

    <!-- Javascript Libs -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/dist/lib/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/dist/lib/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/dist/lib/js/Chart.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/dist/lib/js/bootstrap-switch.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/dist/lib/js/jquery.matchHeight-min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/dist/lib/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/dist/lib/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/dist/lib/js/dataTables.bootstrap.min.js"></script>
    <!-- <script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.12/sorting/numeric-comma.js"></script> -->
    <!-- <script type="text/javascript" src="<?php echo base_url() ?>assets/dist/lib/js/select2.full.min.js"></script> -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/dist/lib/js/ace/ace.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/dist/lib/js/ace/mode-html.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/dist/lib/js/ace/theme-github.js"></script>

    <!-- JQUERY VALIDATE -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery.validate.js"></script>
    <!-- Javascript -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/app2.js"></script>
    <!-- <script type="text/javascript" src="<?php echo base_url() ?>assets/dist/js/index.js"></script> -->
</head>
<style type="text/css">
     #loader{
        background-color: #ffffff;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        position: fixed;
        z-index: 5;
        padding-top: 25%;
        float: left;
        text-align: center;
        overflow-x: hidden;
      }
      .error{
        color: red;
      }

      @media (max-width: 480px) {

      }
</style>
<script type="text/javascript">
    $(window).load(function() {
        $('#loader').fadeOut('slow');
    });
</script> 
<body class="flat-blue" style="font-size: 12px;">
    <div id="loader">
        <img src="<?php echo base_url() ?>/img/45.gif" style="width: 40px;">
        <br>
        <br>
        &nbsp;&nbsp;&nbsp;<label>Mohon tunggu ...</label>
    </div>
    <div class="app-container">
        <div class="row content-container">
            <nav class="navbar navbar-default navbar-fixed-top navbar-top" style="z-index: 1">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-expand-toggle">
                            <i class="fa fa-bars icon"></i>
                        </button>
                        <ol class="breadcrumb navbar-breadcrumb">
                            <li class="active">Dashboard</li>
                        </ol>
                        <button type="button" class="navbar-right-expand-toggle pull-right visible-xs">
                            <i class="fa fa-th icon"></i>
                        </button>
                    </div>
                    <ul class="nav navbar-nav navbar-right">
                        <button type="button" class="navbar-right-expand-toggle pull-right visible-xs">
                            <i class="fa fa-times icon"></i>
                        </button>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-comments-o"></i></a>
                            <ul class="dropdown-menu animated fadeInDown">
                                <li class="title">
                                    Notification <span class="badge pull-right">0</span>
                                </li>
                                <li class="message">
                                    No new notification
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown danger">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-star-half-o"></i> 4</a>
                            <ul class="dropdown-menu danger  animated fadeInDown">
                                <li class="title">
                                    Notification <span class="badge pull-right">4</span>
                                </li>
                                <li>
                                    <ul class="list-group notifications">
                                        <a href="#">
                                            <li class="list-group-item">
                                                <span class="badge">1</span> <i class="fa fa-exclamation-circle icon"></i> new registration
                                            </li>
                                        </a>
                                        <a href="#">
                                            <li class="list-group-item">
                                                <span class="badge success">1</span> <i class="fa fa-check icon"></i> new orders
                                            </li>
                                        </a>
                                        <a href="#">
                                            <li class="list-group-item">
                                                <span class="badge danger">2</span> <i class="fa fa-comments icon"></i> customers messages
                                            </li>
                                        </a>
                                        <a href="#">
                                            <li class="list-group-item message">
                                                view all
                                            </li>
                                        </a>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown profile">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Ferdi Ardiansa <span class="caret"></span></a>
                            <ul class="dropdown-menu animated fadeInDown">
                                <li class="profile-img">
                                    <img src="../img/profile/picjumbo.com_HNCK4153_resize.jpg" class="profile-img">
                                </li>
                                <li>
                                    <div class="profile-info">
                                        <h4 class="username">Emily Hart</h4>
                                        <p>emily_hart@email.com</p>
                                        <div class="btn-group margin-bottom-2x" role="group">
                                            <button type="button" class="btn btn-default"><i class="fa fa-user"></i> Profile</button>
                                            <button type="button" class="btn btn-default"><i class="fa fa-sign-out"></i> Logout</button>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
            <div class="side-menu sidebar-inverse" style="z-index: 2">
                <nav class="navbar navbar-default" role="navigation">
                    <div class="side-menu-container">
                        <div class="navbar-header">
                            <a class="navbar-brand" href="#">
                                <div class="icon fa fa-paper-plane"></div>
                                <div class="title"> SOFTDROID </div>
                            </a>
                            <button type="button" class="navbar-expand-toggle pull-right visible-xs">
                                <i class="fa fa-times icon"></i>
                            </button>
                        </div>
                        <ul class="nav navbar-nav">
                            <li class="active">
                                <a href="<?php echo base_url() ?>index.php/barang/">
                                    <span class="icon fa fa-truck"></span><span class="title">Data Barang</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url() ?>index.php/jenis/">
                                    <span class="icon fa fa-filter"></span><span class="title">Data Jenis</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url() ?>index.php/laporan/">
                                    <span class="icon fa fa-file"></span><span class="title">Laporan</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </nav>
            </div>

            <!-- Main Content -->
            <div class="container-fluid" style="z-index: 4">
                <div class="side-body padding-top">

	                <?php echo $this->load->view($konten, '', TRUE) ?>
                
                </div>
            </div>
        </div>
        <footer class="app-footer">
            <div class="wrapper">
                <span class="pull-right">2.1 <a href="#"><i class="fa fa-long-arrow-up"></i></a></span> © 2015 Copyright.
            </div>
        </footer>
        <div>
</body>

</html>
