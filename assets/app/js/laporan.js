// Validate form with jquery
    $('#form-tambah').validate({
        rules : {
            nama : {
                required : true
            },
            nominal : {
                required : true
            }
        },
        messages : {
            nama : {
                required : 'kolom belum diisi'
            },
            nominal : {
                required : 'kolom belum diisi'
            }
        },
        highlight : function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight : function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        submitHandler : function () {

            var nama              = $('#nama').val();
            var nominal           = $('#nominal').val();
            var pembayaran        = $('input[name="pembayaran"]:checked').val();
            var table             = $('#tabel-laporan').DataTable();

            $.ajax({
                url     : 'http://localhost/portofolio/softdroid/index.php/laporan/insert',
                data    : 'nama='+nama+'&nominal='+nominal+'&pembayaran='+pembayaran,
                type    : 'POST',
                dataType: 'json',
                success : function(data) {
                    if(data == 1) {
                        window.setTimeout(function() {
                            table.ajax.reload();
                            $('#nama').val('');
                            $('#nama').focus();
                            $('#nominal').val('');
                            $('#radio4').checked();
                        }, 2000);
                    }
                }
            });
        }/*end submit handler*/
    });

    jQuery.extend( jQuery.fn.dataTableExt.oSort, {
        "numeric-comma-pre": function ( a ) {
            var x = (a == "-") ? 0 : a.replace( /,/, "." );
            return parseFloat( x );
        },
     
        "numeric-comma-asc": function ( a, b ) {
            return ((a < b) ? -1 : ((a > b) ? 1 : 0));
        },
     
        "numeric-comma-desc": function ( a, b ) {
            return ((a < b) ? 1 : ((a > b) ? -1 : 0));
        }
    } );

    $('#tabel-laporan').DataTable({
        "language": {
                "decimal": ","
        },
        "dom"           : 'tip',
        "processing"    : true,
        "serverside"    : true,
        "ajax"          : { 
                    url : "http://localhost/portofolio/softdroid/index.php/laporan/get_data"},
        "columns"       : [
                {"data"     : "id"},
                {"data"     : "nama"},
                {"data"     : "nominal", render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ') },
                {"data"     : "debit", render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' )},
                {"data"     : "kredit", render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' )}
        ],
            "order": [[ 0, "asc" ]],
            "columnDefs": [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false
                },
                {
                    "targets" : [2],
                    "visible" : false,
                    "searchable": false
                },

                { type: 'numeric-comma', targets: 2 },
                { type: 'numeric-comma', targets: 3 },
                { type: 'numeric-comma', targets: 4 }
        ],
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
        // Total over all pages
            total_kredit = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page
            total_debit = api
                .column( 3, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 3 ).footer() ).html(
                total_debit
            );
            $( api.column( 4 ).footer() ).html(
                total_kredit
            );
        }
    });